
# fedora 24 & fedora 25 on Dell Precision 5510 by deepthought
### October/November 2016

## synopsis
Initially I would have recommended against using this laptop, but after using it for a number of months I have enjoyed this as a primary mobile workstation. If one does not know how to tinker/troubleshoot somewhat under the hood with linux, with system updates or software outside of the main repositories novice users may get stuck. Seems like it would be silly to waste the power of the hardware on the outdated Ubuntu version the laptops ship with. If you're up for tinkering and have the time and inclination to install a newer distro/version and do intermittent debuggin, then it seems like a potentially powerful and productive setup.

## background
Desiring to upgrade a 2013 Macbook Pro and supplement an Arch Linux desktop setup, I decided to try a new distro on a new laptop. I selected the Dell because they ship already with linux (albeit outdated Ubuntu 14), wanted at least a 15" screen, and wanted a powerful setup with a relatively mobile/easy-to-carry-around form factor. I selected Fedora to try something I haven't yet, because RedHat has done a lot for the linux community (despite Fedora being now mainly community-driven afaik), to experiment with some security aspects (SEL), and I enjoyed/despaired at tinkering with RedHat linux on a powerpc approximately 2 decades ago. I have found Fedora to be a bit idiosyncratic compared to Arch, Ubuntu, or even an Amazon EC2 RHEL7 instance. However, it was pleasantly suprising that *most* functions (but definitely not all) worked right out of the box without painful debugging.

## fedora background
Fedora is supported by RedHat, which has used it as a testing environment for what eventually becomes RedHat Enterprise Linux (RHEL). It is a community development distribution that grew out of RedHat Linux, when the latter was discontinued in 2003.

It uses the DNF package manager (vs. e.g. apt on debian/ubuntu). DNF stands for dandified yum (DaNdiFied) and is based off of redhat yum package manager. It uses the RedHat Package Manager system (RPM) and .rpm files (vs. deb files on debian/ubuntu, see https://www.digitalocean.com/community/tutorials/package-management-basics-apt-yum-dnf-pkg). As far as I can tell the default software directory for dnf is `/usr/local`

Security Enhanced Linux (SEL) is a default component of the system including security policies and mandatory access controls.

Default desktop environment is Gnome. GDM = gnome display manager.

Rawhide is the development/testing tree for fedora.

## fedora 24 and fedora 25
I initially installed fedora 24 and then upgraded to fedora 25. The upgrade (specifically, the Wayland switch) broke many things that were essential to me, including:

* brightness and volume keys - I was unable to fix. gnome was able to recognize the keys, they just weren't linked to the proper function. I notice writing to intel backlight system files was able to change the brightness (i.e. at minimum some script hacks would allow using these buttons, I just wasn't in for spending this time)
* redshift - just doesn't work in wayland
* lock screen shortcut - did nothing

To resolve I simply switched the fedora 25 default desktop back to xorg from Wayland and the above started working again. Some/all of these issues might be not be present if you're doing a fresh install, or they may have been fixed in the repos since I last tried wayland.

## initial dell precision 5510 thoughts re hardware/Dell
**pros**:  

* screen looks nice, no clear initial dead pixels
* main parts of power adapter look reasonably designed unlike apple's thin cables which are prone to frequent breakage
* customer service
    * general impression via e.g. reddit is that customer service has historically been bad
    * initial con with initial order disorganization: needless processing delay because they thought credit card information was missing (despite being blatantly present in order)
    * later good service with battery replacement: battery failed after a number of months. after chatting with customer service online they sent out a new battery, and I sent the old one back in the same box. An excellent and relatively easy process. Did *not* anticipate putting customer service under pro, and may have just gotten lucky with my situation.

**neutral / maybe-con**:

* touchpad issues
    * initially the most annoying thing with default settings
    * responsiveness very different from apple
    * initially many missed left clicks & accidental clicks
    * improved dramatically by disabling tap to click and switching clip method to "Fingers"
* others have commented on the difficulty of opening the laptop due to lack of front indendation. I agree that I cannot use one hand to open the laptop, but I'm not sure how different this is from a new MacBook - even though the MacBooks have an identation, at least when they're new there is I think enough resistance to require 2 hands (maybe after time MacBook vs. Precision is easier with wear + indendation). not a huge deal to me.
    
**cons**:  

* fn/ctrl swapped vs. macbook et al. layouts - frustrating mistakes initially. thinking about this more this seems more like bad design - control keys are going to be used generally much more than function modifiers so should be closer to the center of the keyboard (if using pinky and index finger there shouldn't be stretching for copy/paste)
* power strip:
    * no magnetic power - seems like easy to loosen power socket with normal use
    * the attachment of the power cord from computer to the transformer block often comes loose, I found some electric tape keeps this together better
    * the design of the wire from transformer block to computer makes it prone to fraying/damage right where it comes from the block and turns (due to a small clip to try and make it easier to wrap). Placing electric tape to keep it from coming out will reduce the probability of this
* bad speaker design: located on the bottom/front of the computer. Thus there will be reduction in volume due to indirect placement, as well as audio distortion from reflection from variable surfaces (e.g. table vs lap)
* screen is slightly wider and significantly smaller height vs. e.g. 2013 macbook pro - total screen area seems less

## basic setup
### add main user to sudoers

* add main user to sudoers to be able to execute commands as root easily when needed:
  `usermod sampleusername -a -G wheel`  
    * as per https://fedoraproject.org/wiki/Configuring_Sudo

* then restart. or per this can execute gnome quit command (`gnome-session-quit`):  
    * forums.fedoraforum.org/showthread.php?t=305246

### update/upgrade system and using DNF
* to update package lists: `dnf check-update`
* to upgrade installed pacakages: `sudo dnf upgrade`
    * note thaat update & upgrade are the same, with upgrade preferred (see https://dnf.readthedocs.io/en/latest/cli_vs_yum.html#update-and-upgrade-commands-are-the-same)
* searching for packages: `dnf search <search string>`
* searching for pakacages, with searching all fields including description: `dnf search all <search string>`
* see info about a package: `dnf info <package>`
* list dependencies for a package: `dnf repoquery --requires <package>`
* install a package:
   * from repo: `sudo dnf install <package>`
   * multiple packages and assume yes for prompts: `sudo dnf install -y <pacakage 1> <package 2>`
   * from local rpm file: `sudo dnf install <local .rpm file>
* remove a package: `sudo dnf erase <package>`
* `dnf check-update` - lists updates (vs. update?)

refs:  

* https://www.digitalocean.com/community/tutorials/package-management-basics-apt-yum-dnf-pkg
* https://fedoraproject.org/wiki/DNF_system_upgrade
* http://www.cio.com/article/3087878/linux/8-things-to-do-after-installing-fedora-24.html#slide2

### setup repositories

#### RPMFusion
* there does not appear to be a straightforward way to securely download the gpg keys:
    * https://unix.stackexchange.com/questions/106977/how-to-securely-download-rpmfusion-keys
    * http://codesfans.com/ubuntu/GoG-how-to-securely-download-rpmfusion-keys

* got keyid from non-https page and imported from MIT for free/non-free (this is not secure):
    * gpg --keyserver pgp.mit.edu --recv-keys B7546F06
    * gpg --keyserver pgp.mit.edu --recv-keys 96CA6280
 
* at least this site has https instructions:
    * http://rpmfusion.org/Configuration:

    `su -c 'dnf install https://download1.rpmfusion.org/free/fedora/rpmfusion-free-release-$(rpm -E %fedora).noarch.rpm https://download1.rpmfusion.org/nonfree/fedora/rpmfusion-nonfree-release-$(rpm -E %fedora).noarch.rpm'`

#### repo config
* to list active repos: `dnf repolist`
* to disable a repo: `dnf config-manager --set-disabled <repo>`
* after adding repos run `sudo dnf upgrade`

### install/config packages
* install software via repositories:  
    `sudo dnf install gnome-tweak-tool nextcloud-client keepassx pithos vlc thunderbird thunderbird-enigmail redshift qpdfview VirtualBox.x86_64 pidgin pidgin-otr simple-scan redshift gimp wine`

* note for redshift: need location services (otherwise will fail with ambiguous error)  
    https://github.com/jonls/redshift/issues/318

* got battery indicator to show percentage remaining by using dconf editor:
    * switch boolean at: org > gnome > desktop > interface > show-battery-percentage
    * ref: http://www.omgubuntu.co.uk/2016/03/gnome-3-20-new-features-improvements

* chrome:  
    need to download rpm from google and run `sudo dnf install <downloaded rpm>`  
    for netflix or amazon video this probably necessary vs chromium et al. browers due to the DRM components

* atom editor: download and install rpm from https://atom.io/

* re https://www.maketecheasier.com/10-of-the-best-gnome-shell-extensions/  
    good ref re music integration?

### general gnome usage

* to keep an application on top (e.g. chrome): **alt+space** then click always on top  
  source: http://forums.fedoraforum.org/showthread.php?t=267758
* save gnome session on reboot: open dconf editor. under gnome -> gnome-session, switch the auto-save-session flag from default false to true
* you don't have to hit a key before starting to type your password into the lockscreen
* to add programs to favorites, you have to create a desktop entry if one does not exist

### (garden-variety?) gnome thoughts

* good:
    * I like the general design and relative ease of cusomizing the gui. if you like OSX-style app bar or windows-style bar it seems easy to tailor to what you find more useful.
    * initially I hated the top-left corner or meta key way of launching things. after customizing the setup to have an app bar at the bottom of the screen, I've come to appreciate the speed with which you can launch apps with the former method. it's nice to have the flexibility to customize this and once you get used to some of the default features many are nice and ergonomic
* bad:
    * why is tweak tool not integrated in system settings? seems extraordinarily silly to have to use two system settings controllers, wasting time to try and figure out if basic settings are in one or the other. definitely prefer KDE's settings setup.
    * it seems like extensions are mainly/only installed via a web browser - I loathe this and think it's shitty design. what if I want to configure things offline? tough luck? I would also guess that using a web browser to install system extensions is also a much larger attack vector vs. other potential and commonly used methods (assuming browser security and https protocol vs. system app using gpg crypto keys). On just a practical ergonomic level it also sucks: rather than pulling up a system settings page you have to go through the website
    * I started having an issue where my extensions weren't working on boot. They appeared active in the tweak tool but weren't actually functioning. Reinstalling them via removing them and re-adding re-enabled their functionality, until they again weren't there on boot or after unlocking the screen. After upgrading to fedora 25 I haven't yet noticed this again.
    * I see no way to do a slideshow background or screensaver. I've become attached to this since using on OSX and finding this easy to emulate (at least for backgrounds) on an arch desktop with KDE. no dice with gnome, with the one background slideshow app not working at all.

### dual graphics

* I got this working using bumblebee per fedora pages
* I use either primusrun or optirun then application name to launch with discrete GPU
* I know of no way for the system to dynamically switch between CPU/hybrid & discrete GPUs - this would be great
* check available GPUs:

    $ lspci |grep -E "VGA|3D"  
00:02.0 VGA compatible controller: Intel Corporation HD Graphics P530 (rev 06)  
01:00.0 3D controller: NVIDIA Corporation GM107GLM [Quadro M1000M] (rev a2)  

* see the following: 
    * https://fedoraproject.org/wiki/Bumblebee
    * https://wiki.archlinux.org/index.php/Bumblebee
    * https://wiki.archlinux.org/index.php/NVIDIA_Optimus
    * http://askubuntu.com/questions/717465/installing-ubuntu-on-dell-precision-5510

## unresolved issues
### major
    
* gnome: on click and drag folder spring-opens - cannot figure out how to disable this annoying "feature"!
* intermittent inability to connect to bluetooth magicbox speaker. usually able to pair, often unable to connect to make it audio device. resetting the magicbox seems to increase the change of a good pair (but notably this is not necessary with other devices).

    Nov 08 06:48:48 myhost kernel: Bluetooth: hci0: Found Intel DDC parameters: intel/ibt-11-5.ddc  
Nov 08 06:48:48 myhost kernel: Bluetooth: hci0: Applying Intel DDC parameters completed  
Nov 08 06:48:48 myhost kernel: Bluetooth: hci0: Setting Intel event mask failed (-16)  
Nov 08 06:48:48 myhost bluetoothd[1133]: Failed to obtain handles for "Service Changed" characteristic  
Nov 08 06:48:48 myhost bluetoothd[1133]: Endpoint registered: sender=:1.48 path=/MediaEndpoint/A2DPSource  
Nov 08 06:48:48 myhost bluetoothd[1133]: Endpoint registered: sender=:1.48 path=/MediaEndpoint/A2DPSink  
Nov 08 06:48:51 myhost audit[1]: SERVICE_STOP pid=1 uid=0 auid=4294967295 ses=4294967295   subj=system_u:system_r:init_t:s0 msg='unit=systemd-rfkill comm="systemd" exe="/usr/lib/systemd/s  
Nov 08 06:48:55 myhost bluetoothd[1133]: Unable to get Headset Voice gateway SDP record: Device or resource busy  
Nov 08 06:48:55 myhost bluetoothd[1133]: connect error: Device or resource busy (16)  
Nov 08 06:48:57 myhost bluetoothd[1133]: connect error: Device or resource busy (16)  


### medium
* lowest brightness is still very bright at night, even with redshift auto-settings...ideally would have ability to easily do smaller brightness increments. I'm able to change to smaller increments using xrandr, and will probably migrate over a script I wrote for my arch system to use this. note that redshift does not play nice with xrandr brightness adjustments (probably have to use one or the other).
    * see https://wiki.archlinux.org/index.php/backlight
    * some config/control files at /sys/class/backlight
    * modifying the backlight control files doesn't work - only incremental changes seem to work (i.e. trying to write fractions not possible, only integers in preset range)
* wifi seems slow & to intermittently drop vs. other computers - reconnections evident in journalctl (possibility that may be related to my local router configuration, will test more with other setups)
* intermittent very slow text-input, click response, and window resizing in firefox - given the hardware powerhouse this shouldn't be happening unless profound software glitches
    * I think this has improved since around when fedora 25 installed
    * see https://ubuntuforums.org/showthread.php?t=2327162
    * I'm currently investigating whether designating firefox "high priority" in system monitor does anything
* where are custom apps supposed to be installed? e.g. laverna install
   * gnome appears to have trouble finding apps in some directories
   * icon not rendering properly (terminal icon)

### non-major

* background & screensaver slideshow - only found a broken background extension in gnome extension page
* the software GUI in gnome is stating there are updates even though `dnf upgrade` states everything is up
* equivalent of kde-connect for gnome?
* thunderbird does not alert properly - it's supposed to show just a single new email info but shows snippets only of old ones - *probably* not gnomes fault
* music resumes on opening lid (with multiple apps including pithos & spotify)


